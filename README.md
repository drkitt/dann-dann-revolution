![Dann Dann Revolution logo](Dann%20Dann%20Revolution/Dann%20Dann%20Revolution/Content/Graphics/Logo%20ver.%204.png?raw=true)

Dann Dann Revolution is a game that we collaborated to make as a final project for Computer Science 40S at Dakota Collegiate in the 2017/2018 school year. It's here to be viewed by employers as a showcase of our skills. 👩‍💻😎

## Getting started
These instructions will help you get a copy of the project open on your machine so you can play the game.
### Prerequisites
You will need:
- Windows (tested on Windows 10 version 1909)
- Visual Studio for Windows with the C# workload installed (tested with Visual Studio 2019) 
- [Monogame 3.6 for Visual Studio](https://www.monogame.net/2017/03/01/monogame-3-6/) (make sure you have the right version; the game doesn't work on Monogame 3.7 'cause the video player is really finicky)
### Opening the Visual Studio project (and starting the game!)
0. View this repository's Sourcetree page (if you can read this, you're probably already there!).
1. From the sidebar, click the `<Downloads>` button.
2. Click `<Download repository>`.
3. Once the file has finished downloading, use the File Explorer to navigate to your Downloads folder. 
4. In your Downloads folder, locate a file called `<dann-dann-revolution-master.zip>`
5. Extract the .zip file's contents to a convenient location.
6. Open the newly-extracted folder, double-click on the `<dann-dann-revolution-master>` folder, and finally double-click on the `<Dann Dann Revolution>` folder.
7. Double-click on `<Dann Dann Revolution.sln>`
8. Once Visual Studio has opened, press F5 to compile and start the game!

## Authors and acknowledgements
**Dann Chan Productions**
* Vaughn Gregory
* Mike Hodges
* Alex Kitt
* Noah Sweetnam

**Special thanks**
* Mr. Dann, for letting us photoshop his face on everything
* Joshua Smallwood, for helping develop the custom engine
* Mr. Scott, for making the pun that started it all

**Other acknowledgements**
* Songs and associated artwork belong to their respective owners. By section 29 of the Copyright Act of Canada, this project does not infringe copyright, as it was made for educational purposes.
